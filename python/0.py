# import reduce
import datetime
import json
from functools import reduce
from operator import add

import pytz
import requests

tz = pytz.timezone('Asia/Seoul')


# print(str(datetime.datetime.now(tz=tz)))

def ids_to_tags(ids):
    _ids = [
        dict(
            key="id",
            value=_id,
            relation="="
        ) for _id in ids
        ]

    return list(reduce(add, [(elt, dict(operator="OR")) for elt in _ids])[:-1])


app_id = "2f3ccdd6-78e8-4ece-bd0c-25358e1014ea"
key = "NDE5MWQ1MWUtNDJmNC00NTgzLWJjYmMtYmY5YTkyMjA0MTc2"
#title = "타이틀1"
title = "인하산학포털"

#message = "메시지@{}".format(str(datetime.datetime.now(tz=tz)))
message = "테스트 메시지를 보내드립니다.@{}".format(str(datetime.datetime.now(tz=tz)))
message = "[기술개발연구회] 표면처리 기술개발 연구회 (제2차 세미나) 개최 안내"
url = "http://rnd.inha.ac.kr/schedule/view.htm?pageNo=1&scale=10&menuId=21&searchYear=2016&searchMonth=08&menuId=21&searchYear=2016&searchMonth=08&id=3468"
url = "http://rnd.inha.ac.kr/mobile/schedule/view.htm?pageNo=1&scale=5&menuId=24&searchYear=2016&searchMonth=08&menuId=24&searchYear=2016&searchMonth=08&id=3506"
url = "http://rnd.inha.ac.kr/mobile/index.htm"
ids = ['0', '1','jayyun']

tags = ids_to_tags(ids)
#

header = {"Content-Type": "application/json",
          "Authorization": "Basic {}".format(key)}
#
payload = dict(
    app_id=app_id,
    tags=tags,
    headings=dict(en=title),
    contents=dict(en=message),
    data=dict(url=url),
    small_icon='ic_stat_1'
)

data = json.dumps(payload)

print(data)

res = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=data)

print(res.status_code, res.reason, res.text)



print("-"*80)



req = res.request

command = "curl --compressed -X {method} {headers} -d '{data}' '{uri}'"
method = req.method
uri = req.url
data = req.body
headers = ["{0}: {1}".format(k, v) for k, v in req.headers.items()]

headers = map(lambda s:'-H "{}"'.format(s), headers)
headers = " ".join(headers)

print(command.format(method=method, headers=headers, data=data, uri=uri))
