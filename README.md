# about

rest client for node push server ( <https://github.com/Smile-SA/node-pushserver> )

# requirement

    install gradle 
    
<http://gradle.org/>
   

# build

    gradle build

# run sample

    gradle run

# folder structure


    ├── build
    │   ├── distributions                       #binary file
    └── src
        └── main
            └── java
                └── hello
                    ├── PushMessage.java        # data structure
                    ├── PushMessageClient.java  # push message client
                    └── Sample.java             #sample