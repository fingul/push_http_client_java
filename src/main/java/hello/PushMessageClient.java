package hello;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by m on 2/12/16.
 */
public class PushMessageClient {

    static ArrayList<JSONObject> userNameToJSON(ArrayList<String> a) {

        ArrayList<JSONObject> out = new ArrayList<JSONObject>();


        for (String s : a) {
            JSONObject o = new JSONObject();
            o.put("relation", "=");
            o.put("value", s);
            o.put("key", "userId");
            out.add(o);
        }
        return out;

    }


    static void send(String endpoint, PushMessage pushMessage) throws IOException {

        String key = "NTAyZWY1ZDQtYmZkNC00OTcyLThlZjYtY2RjN2NlNjU3ODE2";
        String app_id = "9076ff27-f893-4e6d-8ffb-17b5164f7a84";

        JSONObject obj = getJsonObject(pushMessage, app_id);


        try {
            String jsonResponse;

            URL url = new URL(endpoint);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);

            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Authorization", String.format("Basic %s", key));
            con.setRequestMethod("POST");

            String strJsonBody = obj.toString();


            System.out.println("strJsonBody:\n" + strJsonBody);

            byte[] sendBytes = strJsonBody.getBytes("UTF-8");
            con.setFixedLengthStreamingMode(sendBytes.length);

            OutputStream outputStream = con.getOutputStream();
            outputStream.write(sendBytes);

            int httpResponse = con.getResponseCode();
            System.out.println("httpResponse: " + httpResponse);

            if (httpResponse >= HttpURLConnection.HTTP_OK
                    && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                scanner.close();
            } else {
                Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                scanner.close();
            }
            System.out.println("jsonResponse:\n" + jsonResponse);

        } catch (Throwable t) {
            t.printStackTrace();
        }


//
//        JSONObject ios = new JSONObject();
//        ios.put("badge", 0);
//        ios.put("alert", pushMessage.getMessage());
//        ios.put("sound", "soundName");
//
//        JSONObject payload = new JSONObject();
//        payload.put("url", pushMessage.getUrl());
//        ios.put("payload", payload);
//
//        JSONObject android = new JSONObject();
//
//        JSONObject data = new JSONObject();
//        data.put("message", pushMessage.getMessage());
//        data.put("title", pushMessage.getTitle());
//        data.put("url", pushMessage.getUrl());
//        data.put("image", "icon");
//        android.put("collapseKey", "optional");
//        android.put("data", data);
//        obj.put("android", android);
//        obj.put("ios", ios);

//        System.out.println(obj.toString());
//
//
//        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//
//        try {
//            HttpPost request = new HttpPost(endpoint);
//            request.addHeader("Content-type", "application/json; charset=utf-8");
//            request.addHeader("Authorization", String.format("Basic {}", key));
//            StringEntity params = new StringEntity(obj.toString(), "utf-8");
//            request.setEntity(params);
//            CloseableHttpResponse response = httpClient.execute(request);
//
//            String reasonPhrase = response.getStatusLine().getReasonPhrase();
//            int statusCode = response.getStatusLine().getStatusCode();
//
//            System.out.println(String.format("reasonPhrase=", reasonPhrase));
//            System.out.println(String.format("statusCode=", statusCode));
//
//
//        } finally {
//            try {
//                httpClient.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    private static JSONObject getJsonObject(PushMessage pushMessage, String app_id) {


        JSONObject obj = new JSONObject();

        JSONObject data = new JSONObject();
        data.put("url", pushMessage.getUrl());

        obj.put("data", data);

//        JSONObject contents = new JSONObject();
//        data.put("contents", new JSONObject().put("en", pushMessage.getMessage()));

        obj.put("contents", new JSONObject().put("en", pushMessage.getMessage()));
        obj.put("headings", new JSONObject().put("en", pushMessage.getTitle()));
        obj.put("small_icon", "ic_stat_1");
        obj.put("app_id", app_id);
//        obj.put("app_id", app_id);

        if (!(pushMessage.getUsernames() == null || pushMessage.getUsernames().isEmpty())) {

            ArrayList<JSONObject> tags = userNameToJSON(pushMessage.getUsernames());
            int size = (tags.size() - 1) * 2;
            for (int i = 0; i < size; i += 2) {
                JSONObject o = new JSONObject();
                o.put("operator", "OR");
                tags.add(i + 1, o);
            }
            obj.put("tags", tags);
        }
        return obj;
    }

}
