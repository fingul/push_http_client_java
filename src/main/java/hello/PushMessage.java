package hello;

import java.util.ArrayList;

public class PushMessage {

  ArrayList<String> usernames;
  String url;
  String title;
  String message;

  @Override
  public String toString() {
    return "PushMessage{" +
            "usernames=" + usernames +
            ", url='" + url + '\'' +
            ", title='" + title + '\'' +
            ", message='" + message + '\'' +
            '}';
  }

  public ArrayList<String> getUsernames() {
    return usernames;
  }

  public void setUsernames(ArrayList<String> usernames) {
    this.usernames = usernames;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}