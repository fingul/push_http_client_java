package hello;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Sample {
    public static void main(String[] args) {

        // https://github.com/Smile-SA/node-pushserver#send-a-push
        String endpoint = "https://onesignal.com/api/v1/notifications";

        PushMessage pushMessage = new PushMessage();

        //setUsernames를 호출하지 않으면, 모든 사용자에게 보낸다.
        pushMessage.setUsernames(new ArrayList<String>(Arrays.asList("0","가나다다", "ma104", "jayyun","test")));

        pushMessage.setMessage("내용\n123\n\n456\nABC");
        pushMessage.setTitle("와앱제목제목");
        pushMessage.setUrl("http://google.com");

        try {
            PushMessageClient.send(endpoint, pushMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}